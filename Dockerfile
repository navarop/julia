FROM registry.plmlab.math.cnrs.fr/docker-images/base-notebook:0.0.1

LABEL maintainer="Loic Gouarin <loic.gouarin@polytechnique.edu>"

USER root

ARG julia_version="1.11.1"

RUN apt-get update --yes && \
    apt-get install --yes --no-install-recommends \
    fonts-dejavu \
    gfortran \
    unzip \
    gcc && \
    apt-get clean && rm -rf /var/lib/apt/lists/*


WORKDIR /tmp

ENV JULIA_VERSION="${julia_version}"

RUN set -x && \
    julia_arch=$(uname -m) && \
    julia_short_arch="${julia_arch}" && \
    if [ "${julia_short_arch}" == "x86_64" ]; then \
    julia_short_arch="x64"; \
    fi; \
    julia_installer="julia-${JULIA_VERSION}-linux-${julia_arch}.tar.gz" && \
    julia_major_minor=$(echo "${JULIA_VERSION}" | cut -d. -f 1,2) && \
    mkdir "/opt/julia-${JULIA_VERSION}" && \
    wget -q "https://julialang-s3.julialang.org/bin/linux/${julia_short_arch}/${julia_major_minor}/${julia_installer}" && \
    tar xzf "${julia_installer}" -C "/opt/julia-${JULIA_VERSION}" --strip-components=1 && \
    rm "${julia_installer}" && \
    ln -fs /opt/julia-*/bin/julia /usr/local/bin/julia

USER ${NB_UID}

WORKDIR "${HOME}"

ENV LD_LIBRARY_PATH="$(LD_LIBRARY_PATH):/opt/julia-${JULIA_VERSION}/lib/julia"

RUN julia -e 'import Pkg; Pkg.add("Plots"); Pkg.add("Documenter"); Pkg.add("Aqua")'
